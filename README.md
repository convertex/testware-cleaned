# Test Automation Solution

[ *draft* ]

Agile testing means continuous feedback (CI/CD) from beginning of the development to ensure that quality is really built into the application (QA).

This document is a short description of concepts, methods and tools being considered and used during the elaboration. It is a general overview of the testing workflow in a pragmatic approach.

## Test Planning

Defining time frame and resources required.

Testing should be done by 25/10/2022.

Artifacts:

- test documentation
- test plan
- test framework implementation
- test scripts for automation
- automation pipeline

## Test Analysis

Finding out what to test.

Test approach like

- manual or automated
- risk based, methodical etc.

## Test Design

Selecting the appropriate test technics like

- static and dynamic tests
- specification based and structure based tests
- testing technics checking functional suitability and other quality attributes (e.g. performance efficiency, usability, maintainability)

Considering test technics like

- Boundary Value Analysis
- State Transition Testing
- Use Case Testing
- Statement and Decision Testing and Coverage
- Fault injection for test verification

## Test Implementation

Related to architecture of the application, the Test Automation Solution should be implemented. It includes tasks like

- creating algorithms using a suitable programming language and
- creating test procedure descriptions for the manual tests.

## Test Execution

Test execution is collect test results based on

- scripts running via automation and
- manual procedures.

### Static Testing

- reviewing the documentation (as test base)
- reviewing the code base
  - architecture
  - code convention
  - quality metrics
  - ...
- using IDE's static code analysis feature (syntax errors and warnings)

### Dynamic Testing

Running the testware and evaluate the result. 

#### Test Automation

Structure of Test Automation Solution: *testware*.

## Test Completion

Publishing the test summary report(s).

---
# Evaluating file conversion quality

## Test Analysis for **

