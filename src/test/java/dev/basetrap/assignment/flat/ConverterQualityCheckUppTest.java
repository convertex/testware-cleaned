package dev.basetrap.assignment.flat;

import dev.basetrap.assignment.testcase.TestCase;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = {TestCase.class, FlatFinder.class})
class ConverterQualityCheckUppTest {

    @Autowired
    private TestCase testCase;

    @Test
    public void testCorrectJzonFileNames() {
        testCase.testCorrectJzonFileName();
    }

    @Test
    public void testJzonFileExt() {
        testCase.testJzonFileExt();
    }

    @Test
    void testCorrectFlatFileNames() {
        testCase.testCorrectFlatFileName();
    }

    @Test
    void testFLatFileExt() {
        testCase.testFLatFileExt();
    }

    @Test
    void testNumberOfFieldsInOutput() {
        testCase.testNumberOfFieldsInOutput();
    }

    @Test
    void testMapping() {
        testCase.testMapping();
    }

    @Test
    void testFileNameAgainstID() {
        testCase.testFileNameAgainstIDFlat();
    }
}