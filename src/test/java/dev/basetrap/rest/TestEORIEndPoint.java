package dev.basetrap.rest;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import static io.restassured.RestAssured.given;
import static io.restassured.config.RedirectConfig.redirectConfig;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class TestEORIEndPoint {

    Logger log = Logger.getLogger(TestEORIEndPoint.class.getName());

    private static final String BASE_URI_STRING = "https://test-api.service.hmrc.gov.uk/";

    //    @Test
    public void testIfServerAlive() {
        Response helloResponse = given()
                .header("Accept", "application/vnd.hmrc.1.0+json")
                .when()
                .get("/hello/world")
                .then()
                .statusCode(200).extract().response();
    }

//    @Test
    public void path_requested() throws JSONException {
        JSONArray eoris = new JSONArray();
        eoris.put("GB182739473232");
        eoris.put("GB123456789136");
        eoris.put("GB8392848394939");

        JSONObject requestBody = new JSONObject();
        requestBody.put("eoris", eoris);

        // This endpoint is open access and requires no Authorization header.

        Map<String, Object> headerMap = new HashMap<String, Object>();
        headerMap.put("Accept", "application/vnd.hmrc.1.0+json");
        headerMap.put("Content-Type", "application/json");
        headerMap.put("User-Agent", "Apache-HttpClient/4.5.5 (Java/16.0.1)");
        headerMap.put("Access-Control-Allow-Origin", "https://test-api.service.hmrc.gov.uk/");

//        RequestSpecBuilder builder = new RequestSpecBuilder();
//        builder.setBody(requestBody.toString());
//        builder.setContentType("application/json");
//        builder.setAccept("application/vnd.hmrc.1.0+json");

//        RequestSpecification requestSpec = builder.build();
        Response response = given()
                .body(requestBody.toString())
                .headers(headerMap)
                .config(RestAssured.config().redirect(redirectConfig().followRedirects(true)))
                .when()
                .post("/customs/eori/lookup/check-multiple-eori")
                .then()
                .extract().response();

        log.info(response.getBody().asPrettyString());
        try {

            assertThat(response.statusCode(), equalTo(200));

        } catch (AssertionError e) {
            System.out.println(e.getMessage());
        }
    }
}