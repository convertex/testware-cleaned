package dev.basetrap.assignment.flat;

import dev.basetrap.assignment.util.QualityCheckLogMessage;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Properties;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static dev.basetrap.assignment.util.QualityCheckLogMessage.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class FlatParser {

    public static final String END_OF_THE_ROWAD = " #";

    public static final String EMTY_RANGE_MARK = "\\s*";

    //    private String PROP_FLAT_ROW_PATTERN;
    public static final String PROP_FLAT_ROW_PATTERN = "^[A-Z]{3}\\ \\| .*\\ #$";

    private static Logger log = Logger.getLogger(FlatParser.class.getName());

    private static String[] fields;

    private static boolean isMessageRequiredForPositiveCases = Boolean.TRUE;

    public FlatParser() {
        try (InputStream input = new FileInputStream("src/main/resources/config.properties")) {
            Properties properties = new Properties();
            properties.load(input);

//            PROP_FLAT_ROW_PATTERN = properties.getProperty("flat.line.pattern");

        } catch (IOException io) {
            io.printStackTrace();
        }

    }

    public static void extractFields(String line) {
        if (line.length() > 0) {
            fields = line
                    .substring(0, line.length() - (END_OF_THE_ROWAD.length() - 1))
                    .split("\\|");
        }
    }

    public static boolean isRowRegexpMatching(String line) {
        Pattern pattern = Pattern.compile(PROP_FLAT_ROW_PATTERN);
        Matcher matcher = pattern.matcher(line);
        return matcher.matches();
    }

    public static String getField(int number) {
        String aField = "";
        String aFieldTrimmed = "";
        if (null != fields) {
            aField = fields[number];
            aFieldTrimmed = fields[number].trim();
        }

        if (aFieldTrimmed.length() > 0) {
            return aFieldTrimmed;
        }

        return aField;
    }

    public int getNumberOfFields() {
        if (fields != null) {
            return fields.length;
        } else {
            log.info(QualityCheckLogMessage.ERROR_ROW_MISSING);
            return 0;
        }
    }

    public static void checkNumberOfFields(String flatFileDirectory, String flatFile) {
        List<String> allFlatLines;
        try {
            allFlatLines = Files.readAllLines(Paths.get(flatFileDirectory + flatFile));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        log.info(System.lineSeparator());

        for (String aFlatLine : allFlatLines) {
            checkNumberOfFieldsForTheCurrentLine(aFlatLine);

        }
        assertThat(flatFile, is(not(emptyString())));
    }


    private static String getDecoratedSectionName(String sectionName) {
        return "[ " + sectionName + " ] ";
    }

    public static void checkNumberOfFieldsForTheCurrentLine(String aFlatLine) {

        FlatParser.extractFields(aFlatLine);
        String sectionName = FlatParser.getField(FlatFile.SECTION_NAME_POS);

        FlatParser flatParser = new FlatParser();
        if (1 > aFlatLine.length()) {
            log.info(EMPTY_SECTION_NAME + ERROR_EMPTY_FLAT_LINE);
            return;
        } else if (!flatParser.isRowRegexpMatching(aFlatLine)) {
            log.info(EMPTY_SECTION_NAME + ERROR_ROW_PATTERN);
            return;
        }

        int acceptedNumberOfFields = 0;
        int actualNumberOfFields = 0;
        try {
            if ((FlatFile.SECTION_NAME.STR.name()).equals(sectionName)) {
                acceptedNumberOfFields = FlatFile.STR_FIELDS.values().length;
                actualNumberOfFields = flatParser.getNumberOfFields();
                assertThat(acceptedNumberOfFields, equalTo(actualNumberOfFields));
                if (isMessageRequiredForPositiveCases) {
                    log.info(getDecoratedSectionName(sectionName)
                            + CHK_NUMBER_OF_FIELDS + flatParser.getNumberOfFields());
                }
            } else if ((FlatFile.SECTION_NAME.GEN.name()).equals(sectionName)) {
                acceptedNumberOfFields = FlatFile.GEN_FIELDS.values().length;
                actualNumberOfFields = flatParser.getNumberOfFields();
                assertThat(acceptedNumberOfFields, equalTo(actualNumberOfFields));
                if (isMessageRequiredForPositiveCases) {
                    log.info(getDecoratedSectionName(sectionName)
                            + CHK_NUMBER_OF_FIELDS + flatParser.getNumberOfFields());
                }
            } else if ((FlatFile.SECTION_NAME.TEC.name()).equals(sectionName)) {
                acceptedNumberOfFields = FlatFile.TEC_FIELDS.values().length;
                actualNumberOfFields = flatParser.getNumberOfFields();
                assertThat(acceptedNumberOfFields, equalTo(actualNumberOfFields));
                if (isMessageRequiredForPositiveCases) {
                    log.info(getDecoratedSectionName(sectionName)
                            + CHK_NUMBER_OF_FIELDS + flatParser.getNumberOfFields());
                }
            } else if ((FlatFile.SECTION_NAME.DET.name()).equals(sectionName)) {
                acceptedNumberOfFields = FlatFile.DET_FIELDS.values().length;
                actualNumberOfFields = flatParser.getNumberOfFields();
                assertThat(acceptedNumberOfFields, equalTo(actualNumberOfFields));
                if (isMessageRequiredForPositiveCases) {
                    log.info(getDecoratedSectionName(sectionName)
                            + CHK_NUMBER_OF_FIELDS + flatParser.getNumberOfFields());
                }
            } else if ((FlatFile.SECTION_NAME.END.name()).equals(sectionName)) {
                acceptedNumberOfFields = FlatFile.END_FIELDS.values().length;
                actualNumberOfFields = flatParser.getNumberOfFields();
                assertThat(acceptedNumberOfFields, equalTo(actualNumberOfFields));
                if (isMessageRequiredForPositiveCases) {
                    log.info(getDecoratedSectionName(sectionName)
                            + CHK_NUMBER_OF_FIELDS + flatParser.getNumberOfFields());
                }
            }
        } catch (
                Throwable t) {
            log.info(getDecoratedSectionName(sectionName)
                    + FAILURE_NUMBER_OF_FIELDS
                    + acceptedNumberOfFields
                    + PLUS_CENTERED
                    + actualNumberOfFields
            );

//            log.info(EMPTY_SECTION_NAME + ERROR_NUMBER_OF_FIELDS + aFlatLine);
        }

    }

    public static void setMessageRequiredForPositiveCases(boolean messageRequiredForPositiveCases) {
        isMessageRequiredForPositiveCases = messageRequiredForPositiveCases;
    }
}