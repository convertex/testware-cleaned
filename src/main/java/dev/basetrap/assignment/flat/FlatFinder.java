package dev.basetrap.assignment.flat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.logging.Logger;

@Component
public class FlatFinder {

    private Logger log = Logger.getLogger(FlatFinder.class.getName());

    private static String fileName;

    private static String filed;

    @Autowired
    public FlatFinder() {
    }

    public String lookUpFlatFileByID(String flatFileDirectory, List<String> flatFileNamesAll, String searchKey) {

        for (String flatFileName : flatFileNamesAll) {

            List<String> allFlatLines;
            try {
                allFlatLines = Files.readAllLines(Paths.get(flatFileDirectory + flatFileName));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

            // key-based strategy
//            // connecting input file with output file based on input ID and output ID
            String flatFileLineContainingId = getLineBySectionName(allFlatLines, FlatFile.SECTION_NAME.STR.name());
            if (isIDFound(flatFileLineContainingId, searchKey)) {
                return flatFileName;
            }
        }
        return "";
    }

    private static boolean isIDFound(String flatFileName, String searchKey) {
        if (searchKey == null) {
            return Boolean.FALSE;
        }
        return flatFileName.contains(searchKey);
    }

    public String lookUpFlatFileByIDStrict(String flatFileDirectory, List<String> flatFileNamesAll, String searchKey) {

        for (String flatFileName : flatFileNamesAll) {

            List<String> allFlatLines;
            try {
                allFlatLines = Files.readAllLines(Paths.get(flatFileDirectory + flatFileName));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

            // pattern-based strategy
            // connecting input file with output file based on input ID and output ID
            String flatFileLineContainingId = getLineBySectionName(allFlatLines, FlatFile.SECTION_NAME.STR.name());

            FlatParser.extractFields(flatFileLineContainingId);
            String flatLineField = FlatParser.getField(FlatFile.STR_FIELDS.ID.ordinal());
            if (flatLineField.equals(searchKey)) {
                return flatFileName;
            }

        }
        return "";
    }

    private static String getLineBySectionName(List<String> lines, String searchKey) {

        FlatParser flatParser = new FlatParser();
        for (String line : lines) {
            flatParser.extractFields(line);
            String flatLineField = flatParser.getField(FlatFile.ROW.SECTION_NAME.ordinal());
            if (!flatLineField.equals("")) {
                return line;
            }
        }
        return "";
    }
}
