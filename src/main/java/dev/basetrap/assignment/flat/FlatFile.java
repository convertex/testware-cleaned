package dev.basetrap.assignment.flat;

import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static dev.basetrap.assignment.util.QualityCheckLogMessage.*;

public class FlatFile {

    private static Logger log = Logger.getLogger(FlatFile.class.getName());

    public static final int SECTION_NAME_POS = 0;

    public static boolean checkFlatLineRegexp(String aFlatLine, String regexp) {
        Pattern pattern = Pattern.compile(regexp);
        Matcher matcher = pattern.matcher(aFlatLine);
        if (!matcher.matches()) {

            if (1 > aFlatLine.length()) {
                log.info(EMPTY_SECTION_NAME + ERROR_EMPTY_FLAT_LINE);
            } else {
                log.info(EMPTY_SECTION_NAME + ERROR_ROW_PATTERN + PLUS_CENTERED + aFlatLine);
            }
        }
        return matcher.matches();
    }

    public static boolean isOutputFileNameFullRegexpMatching(String text, String fileNameRegexp, boolean isSilent) {
        Pattern pattern = Pattern.compile(fileNameRegexp);
        Matcher matcher = pattern.matcher(text);
        if (matcher.matches()) {
            if (!isSilent) log.info(CHK_FILE_NAME_FLAT + text);
        } else {
            if (!isSilent) log.info(ERROR_FILE_NAME_PATTERN_FLAT + text);
        }
        return matcher.matches();
    }

    public static boolean checkRegexpOutputFileExt(String text, String expression, boolean isSilent) {
        Pattern pattern = Pattern.compile(expression);
        Matcher matcher = pattern.matcher(text);
        if (matcher.matches()) {
            if (!isSilent) log.info(CHK_FILE_NAME_EXT + text);
        } else {
            if (!isSilent) log.info(ERROR_FILE_EXT_PATTERN_FLAT + text);
        }
        return matcher.matches();
    }

    public enum SECTION_NAME {
        STR,
        GEN,
        TEC,
        DET,
        END
    }

    public enum ROW {
        SECTION_NAME
    }

    public enum STR_FIELDS {
        STR,
        ID,
        DATE
    }

    public enum GEN_FIELDS {
        GEN,
        COUNTRY,
        DIVISION,
        STATUS
    }

    public enum TEC_FIELDS {
        TEC,
        DOCUMENTS,
        NUMBER_OF_PAGES,
        ERR
    }

    public enum DET_FIELDS {
        DET,
        CLERK,
        TRADER,
        INPUT_TIME,
        MESSAGE
    }

    public enum END_FIELDS {
        END,
        END_TIME
    }

    public static final String SUBMITTED = "SUBMITTED";

    public static final String MISSING_DATA = "MISSING DATA";

    public static final String PROCESSING_FAILED = "PROCESSING FAILED";
    public static final String NONE = "None";

}
