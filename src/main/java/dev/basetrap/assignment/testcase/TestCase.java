package dev.basetrap.assignment.testcase;

import dev.basetrap.assignment.flat.FlatFile;
import dev.basetrap.assignment.flat.FlatFinder;
import dev.basetrap.assignment.flat.FlatParser;
import dev.basetrap.assignment.jzon.JzonFile;
import dev.basetrap.assignment.mapping.Mapper;
import dev.basetrap.assignment.mapping.SingleID;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static dev.basetrap.assignment.util.QualityCheckLogMessage.*;

@Component
public class TestCase {

    public static final boolean SILENT_MODE = Boolean.TRUE;
    public static final boolean NON_SILENT_MODE = !SILENT_MODE;

    private static final Logger log = LogManager.getLogger(TestCase.class);

    private static String PROP_JZON_FILE_NAME_REGEXP;

    public static String PROP_FLAT_FILE_NAME_REGEXP;
    public static String PROP_JZON_FILE_EXT_REGEXP;
    public static String PROP_FLAT_FILE_EXT_REGEXP;

    public static String FLAT_FILE_DIRECTORY;

    private String flatFileDirectory;

    private String jzonDirectory;

    public SingleID singleID;

    @Autowired
    private FlatFinder flatFinder;

    @Autowired
    public TestCase() {

//        singleID = SingleID.getInstance();

        String basePath = new File("").getAbsolutePath();

        try (InputStream input = new FileInputStream("src/main/resources/config.properties")) {
            Properties properties = new Properties();
            properties.load(input);

            PROP_JZON_FILE_NAME_REGEXP = properties.getProperty("jzon.file.name.regexp");
            PROP_FLAT_FILE_NAME_REGEXP = properties.getProperty("flat.file.name.regexp");

            PROP_FLAT_FILE_EXT_REGEXP = properties.getProperty("flat.file.ext.regexp");
            PROP_JZON_FILE_EXT_REGEXP = properties.getProperty("jzon.file.ext.regexp");


            jzonDirectory = new File(basePath
                    + File.separator + properties.getProperty("data.sample.input")).getAbsolutePath()
                    + File.separator;

            flatFileDirectory = new File(basePath
                    + File.separator
                    + properties.getProperty("data.sample.output")).getAbsolutePath()
                    + File.separator;

            FLAT_FILE_DIRECTORY = flatFileDirectory;

        } catch (IOException io) {
            io.printStackTrace();
        }
    }

    public void testCorrectJzonFileName() {
        collectJzonFileNamesVerified(NON_SILENT_MODE);
    }

    public void testCorrectFlatFileName() {
        collectFlatFileNamesVerified(NON_SILENT_MODE);
    }

    public void testNumberOfFieldsInOutput() {

        List<String> correctFlatFileNames = collectFlatFileNamesVerified(SILENT_MODE);

        for (String aFlatFileName : correctFlatFileNames) {
            log.info(CHK_FILE + aFlatFileName);
            FlatParser.checkNumberOfFields(flatFileDirectory, aFlatFileName);
            log.info(System.lineSeparator());
//            log.info(System.lineSeparator());
        }
    }

    private int reportCount = 0;

    public void testMapping() {

        List<String> jzonFileNamesAll = collectJzonFileNamesAll();
        List<String> flatFileNamesAll = collectFlatFileNamesAll();

        Mapper mapper = new Mapper(flatFileDirectory, jzonDirectory);

        for (String aJzonFileName : jzonFileNamesAll) {

            log.info("< REPORT_START_" + String.format("%03d", reportCount++) + " >");
            log.info(System.lineSeparator());

            String thePairedFlatFileName = pairByJzonID(aJzonFileName, flatFileNamesAll);

            boolean OK_PAIR_BY_ID = thePairedFlatFileName.length() > 0;

            boolean OK_JZON_FILE_NAME_REGEXP
                    = JzonFile.checkRegexpInput(aJzonFileName, PROP_JZON_FILE_NAME_REGEXP, NON_SILENT_MODE);
            boolean OK_FLAT_FILE_NAME_REGEXP
                    = FlatFile.isOutputFileNameFullRegexpMatching(thePairedFlatFileName, PROP_FLAT_FILE_NAME_REGEXP, NON_SILENT_MODE);

            boolean IDS_EQUALS_IN_FILE_NAMES = checkNameOfFilesToBePaired(aJzonFileName, thePairedFlatFileName);


            if (OK_PAIR_BY_ID && OK_FLAT_FILE_NAME_REGEXP) {
//                log.info(System.lineSeparator());
                log.info(CHK_FILE + aJzonFileName + PLUS_CENTERED + thePairedFlatFileName);
                log.info(System.lineSeparator());
            }

            if (OK_PAIR_BY_ID && !IDS_EQUALS_IN_FILE_NAMES) {
//                log.info(System.lineSeparator());
                log.info(ERROR_PAIRED_WITH_IRREGULAR_FILE_NAME + PLUS_CENTERED + thePairedFlatFileName);
                log.info(System.lineSeparator());
            }

            if (aJzonFileName.equals("") || thePairedFlatFileName.equals("")) {
                log.info(ERROR_FILE
                        + ((aJzonFileName.equals("")) ? MISSING_FIELD : aJzonFileName)
                        + PLUS_CENTERED
                        + (thePairedFlatFileName.equals("") ? MISSING_FIELD : thePairedFlatFileName)
                );
                log.info(System.lineSeparator());
            }

            if (OK_PAIR_BY_ID) {
                mapper.compareFieldsOfPairedFiles(aJzonFileName, thePairedFlatFileName);
            }

            log.info(System.lineSeparator());
            log.info("< REPORT_END >");
            log.info(System.lineSeparator());
        }
    }

    private boolean checkNameOfFilesToBePaired(String aJzonFileName, String pairedFlatFile) {

        boolean fileNamesMatchingOnIDs = aJzonFileName.split("\\.")[0]
                .equals(pairedFlatFile.split("\\_")[0]);

        return fileNamesMatchingOnIDs;
    }

    private List<String> collectJzonFileNamesVerified(boolean isSilent) {
        List<String> correctJzonFileNames = new ArrayList<>();

        List<String> jzonFileNames = Stream.of(new File(jzonDirectory).listFiles())
                .filter(file -> !file.isDirectory())
                .map(File::getName)
                .collect(Collectors.toList());

        for (String fileName : jzonFileNames) {
            if (JzonFile.checkRegexpInput(fileName, PROP_JZON_FILE_NAME_REGEXP, isSilent)) {
                correctJzonFileNames.add(fileName);
            }
        }
        return correctJzonFileNames;
    }

    private List<String> collectFlatFileNamesVerified(boolean isSilent) {
        List<String> correctFlatFileNames = new ArrayList<>();

        List<String> flatFileNames = Stream.of(new File(flatFileDirectory).listFiles())
                .filter(file -> !file.isDirectory())
                .map(File::getName)
                .collect(Collectors.toList());

        for (String fileName : flatFileNames) {
            if (FlatFile.isOutputFileNameFullRegexpMatching(fileName, PROP_FLAT_FILE_NAME_REGEXP, isSilent)) {
                correctFlatFileNames.add(fileName);
            }
        }
        return correctFlatFileNames;
    }

    private List<String> collectJzonFileNamesAll() {
        return Stream.of(new File(jzonDirectory).listFiles())
                .filter(file -> !file.isDirectory())
                .map(File::getName)
                .collect(Collectors.toList());
    }

    private List<String> collectFlatFileNamesAll() {
        List<String> flatFileNames = Stream.of(new File(flatFileDirectory).listFiles())
                .filter(file -> !file.isDirectory())
                .map(File::getName)
                .collect(Collectors.toList());
        return flatFileNames;
    }

    private String pairByJzonID(String aJzonFileName, List<String> flatFileNamesAll) {

        JSONParser jsonParser = new JSONParser();
        JSONArray jsonArray = null;
        try {
            jsonArray = (JSONArray) jsonParser.parse(new FileReader(jzonDirectory + aJzonFileName));
        } catch (Exception e) {
            e.printStackTrace();
        }

        JSONObject jzonObject = (JSONObject) jsonArray.get(JzonFile.ARRAY_INDEX_OF_OBJECT_TO_BE_PROCESSED);

        if (null != jzonObject.get(JzonFile.ID)) {
            String jzonField = jzonObject.get(JzonFile.ID).toString();
            return flatFinder.lookUpFlatFileByID(flatFileDirectory, flatFileNamesAll, jzonField);
        } else {
            return "";
        }
    }

    public void testFileNameAgainstIDFlat() {

        List<String> flatFileNamesAll = collectFlatFileNamesAll();

        for (String aFlatFileName : flatFileNamesAll) {

            String idInFile;
            boolean isMatchingRegexp = (null != (idInFile = extractIdFromFlatFile(aFlatFileName)));
            if (!isMatchingRegexp) {
                log.info(ERROR_ROW_PATTERN + aFlatFileName);
                continue;
            }

            boolean isIDInFile = idInFile.length() > 0;

            String idInFileName;
            boolean isIDInFileName = !(null == (idInFileName = extractIdFromFlatFileName(aFlatFileName)));

            if (!isIDInFile) {
                log.info("HELLO");
            } else if (!isIDInFileName) {
                log.info(EXCEPTION_UNMATCHING_ID_AND_FILENAME
                        + aFlatFileName
                );
            } else {
                String flatFileByIdInFile = flatFinder.lookUpFlatFileByID(flatFileDirectory,
                        flatFileNamesAll,
                        idInFile);
                log.info(CHK_FILE_NAME_TO_ID + flatFileByIdInFile);
            }
        }
    }

    public void testJzonFileExt() {

        List<String> jzonFileNamesAll = collectJzonFileNamesAll();

        for (String fileName : jzonFileNamesAll) {
            boolean isFileExtRegexMatched = JzonFile.checkRegexpJzonFileExt(fileName, PROP_JZON_FILE_EXT_REGEXP, SILENT_MODE);
            if (isFileExtRegexMatched) {
                log.info(CHK_JZON_FILE_EXT + fileName);
            } else {
                log.info(ERROR_FILE_EXT_PATTERN_JZON + fileName);
            }
        }
    }

    public void testFLatFileExt() {

        List<String> flatFileNamesAll = collectFlatFileNamesAll();

        for (String fileName : flatFileNamesAll) {
            boolean isFileExtRegexMatched = FlatFile.checkRegexpOutputFileExt(fileName, PROP_FLAT_FILE_EXT_REGEXP, SILENT_MODE);
            if (isFileExtRegexMatched) {
                log.info(CHK_FLAT_FILE_EXT + fileName);
            } else {
                log.info(ERROR_FILE_EXT_PATTERN_FLAT + fileName);
            }
        }
    }

    private String extractIdFromFlatFileName(String flatFileName) {
        // @ToDo Not so. Compute from definition!
        // flat file name ID length
        int idLength = 11;
        if (!(flatFileName.length() < idLength)) {
            return flatFileName.substring(0, 11);
        } else {
            return null;
        }
    }


    public static String extractIdFromFlatFile(String aFlatFileName) {
        List<String> allFlatLines;
        try {
            allFlatLines = Files.readAllLines(Paths.get(FLAT_FILE_DIRECTORY + aFlatFileName));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        for (String aFlatLine : allFlatLines) {
            boolean isRegexpMatching = FlatParser.isRowRegexpMatching(aFlatLine);
            if (!isRegexpMatching) {
                return null;
            }

            FlatParser.extractFields(aFlatLine);
            String sectionNameOfTheCurrentFlatLine = FlatParser.getField(FlatFile.SECTION_NAME_POS);
            if ((FlatFile.SECTION_NAME.STR.name()).equals(sectionNameOfTheCurrentFlatLine)) {
                return FlatParser.getField(FlatFile.STR_FIELDS.ID.ordinal());
            }
        }
        return "";
    }

}
