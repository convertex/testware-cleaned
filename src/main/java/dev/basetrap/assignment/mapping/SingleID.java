package dev.basetrap.assignment.mapping;

public class SingleID {

    private static SingleID single_instance = null;

    private String inputFileID;

    private String outputFileID;

    private SingleID() {
    }

    public static SingleID getInstance() {
        if (single_instance == null)
            single_instance = new SingleID();

        return single_instance;
    }

    public String getInputFileID() {
        return inputFileID;
    }

    public void setInputFileID(String inputFileID) {
        this.inputFileID = inputFileID;
    }

    public String getOutputFileID() {
        return outputFileID;
    }

    public void setOutputFileID(String outputFileID) {
        this.outputFileID = outputFileID;
    }
}
