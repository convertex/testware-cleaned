package dev.basetrap.assignment.mapping;

import dev.basetrap.assignment.flat.FlatFile;
import dev.basetrap.assignment.flat.FlatParser;
import dev.basetrap.assignment.jzon.JzonFile;
import dev.basetrap.assignment.util.QualityCheckLogMessage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static dev.basetrap.assignment.util.QualityCheckLogMessage.*;

public class Mapper {

    private static final Logger log = LogManager.getLogger(Mapper.class);

    private String jzonDirectory;

    private String flatFileDirectory;

    public Mapper(String flatFileDirectory, String jzonDirectory) {
        this.flatFileDirectory = flatFileDirectory;
        this.jzonDirectory = jzonDirectory;
    }

    public void compareFieldsOfPairedFiles(String aJzonFileName, String aFlatFileName) {

        if (aFlatFileName.equals("")) {
            log.info(ERROR_MISSING_FLAT_FILE_FOR + aJzonFileName);
            return;
        }

        List<String> allFlatLines;
        try {
            allFlatLines = Files.readAllLines(Paths.get(flatFileDirectory + aFlatFileName));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        JSONParser parser = new JSONParser();
        JSONArray jsonArray;
        try {
            jsonArray = (JSONArray) parser.parse(new FileReader(jzonDirectory + aJzonFileName));
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }


        for (String aFlatLine : allFlatLines) {


            boolean shouldBeContinued = FlatFile.checkFlatLineRegexp(aFlatLine, FlatParser.PROP_FLAT_ROW_PATTERN);
            if (!shouldBeContinued) {
                continue;
            }

            FlatParser.setMessageRequiredForPositiveCases(Boolean.FALSE);
            FlatParser.checkNumberOfFieldsForTheCurrentLine(aFlatLine);

            FlatParser.extractFields(aFlatLine);
            String sectionNameOfTheCurrentFlatLine = FlatParser.getField(FlatFile.SECTION_NAME_POS);

            JSONObject jsonObject = (JSONObject) jsonArray.get(JzonFile.ARRAY_INDEX_OF_OBJECT_TO_BE_PROCESSED);
            String jzonField = null;
            String flatLineField = "";
            if ((FlatFile.SECTION_NAME.STR.name()).equals(sectionNameOfTheCurrentFlatLine)) {

                String sectionName = "[ " + FlatParser.getField(FlatFile.STR_FIELDS.STR.ordinal()) + " ] ";

                if (null != jsonObject.get(JzonFile.ID)) {
                    jzonField = jsonObject.get(JzonFile.ID).toString();
                }
                flatLineField = FlatParser.getField(FlatFile.STR_FIELDS.ID.ordinal());
                log.info(sectionName + performMapCheckWithLogMessage(jzonField, flatLineField));

                if (null != jsonObject.get(JzonFile.DATE)) {
                    jzonField = jsonObject.get(JzonFile.DATE).toString();
                }
                flatLineField = FlatParser.getField(FlatFile.STR_FIELDS.DATE.ordinal());
                log.info(sectionName + performMapCheckWithLogMessage(jzonField, flatLineField));

            } else if ((FlatFile.SECTION_NAME.GEN.name()).equals(sectionNameOfTheCurrentFlatLine)) {

                String sectionName = "[ " + FlatParser.getField(FlatFile.GEN_FIELDS.GEN.ordinal()) + " ] ";


                String textAfterCountryCodeInJzon = "";
                if (null != jsonObject.get(JzonFile.COUNTRY)) {
                    int countryCodeLength = 2;
                    textAfterCountryCodeInJzon = jsonObject.get(JzonFile.COUNTRY).toString().substring(countryCodeLength);
                    jzonField = extractCountryCodeFromJzonField(jsonObject.get(JzonFile.COUNTRY).toString());
                }
                flatLineField = FlatParser.getField(FlatFile.GEN_FIELDS.COUNTRY.ordinal());
                log.info(sectionName + performMapCheckWithLogMessage(jzonField,
                        flatLineField,
                        null,
                        textAfterCountryCodeInJzon));

                if (null != jsonObject.get(JzonFile.DIVISION)) {
                    jzonField = jsonObject.get(JzonFile.DIVISION).toString();
                }
                flatLineField = FlatParser.getField(FlatFile.GEN_FIELDS.DIVISION.ordinal());
                log.info(sectionName + performMapCheckWithLogMessage(jzonField, flatLineField));

                if (null != jsonObject.get(JzonFile.STATUS)) {
                    jzonField = jsonObject.get(JzonFile.STATUS).toString();
                }
                flatLineField = FlatParser.getField(FlatFile.GEN_FIELDS.STATUS.ordinal());
                log.info(sectionName + performMapCheckWithLogMessage(jzonField, flatLineField));

            } else if ((FlatFile.SECTION_NAME.TEC.name()).equals(sectionNameOfTheCurrentFlatLine)) {

                String sectionName = "[ " + FlatParser.getField(FlatFile.TEC_FIELDS.TEC.ordinal()) + " ] ";

                if (null != jsonObject.get(JzonFile.INVOICES_COUNT)) {
                    jzonField = jsonObject.get(JzonFile.INVOICES_COUNT).toString();
                }
                flatLineField = FlatParser.getField(FlatFile.TEC_FIELDS.DOCUMENTS.ordinal());
                log.info(sectionName + performMapCheckWithLogMessage(jzonField, flatLineField));

                if (null != jsonObject.get(JzonFile.PAGES_COUNT)) {
                    jzonField = jsonObject.get(JzonFile.PAGES_COUNT).toString();
                }
                flatLineField = FlatParser.getField(FlatFile.TEC_FIELDS.NUMBER_OF_PAGES.ordinal());
                log.info(sectionName + performMapCheckWithLogMessage(jzonField, flatLineField));

                log.info(sectionName + CHK_MAPPING_FIELDS_TARGET_ONLY + EXPECTED_EMPTY_FLAT_FIELD_RANGE_MARKER);

            } else if ((FlatFile.SECTION_NAME.DET.name()).equals(sectionNameOfTheCurrentFlatLine)) {

                String sectionName = "[ " + FlatParser.getField(FlatFile.DET_FIELDS.DET.ordinal()) + " ] ";

                if (null != jsonObject.get(JzonFile.ASSIGNEE)) {
                    jzonField = extractAssigneeFromJzonField(jsonObject.get(JzonFile.ASSIGNEE).toString());
                }
                flatLineField = FlatParser.getField(FlatFile.DET_FIELDS.CLERK.ordinal());
                log.info(sectionName + performMapCheckWithLogMessage(jzonField,
                        flatLineField,
                        null,
                        EMAIL_AT_DOMAIN));

                if (null != jsonObject.get(JzonFile.EXPORTER)) {
                    jzonField = jsonObject.get(JzonFile.EXPORTER).toString();
                }
                flatLineField = FlatParser.getField(FlatFile.DET_FIELDS.TRADER.ordinal());
                log.info(sectionName + performMapCheckWithLogMessage(jzonField, flatLineField));


                flatLineField = FlatParser.getField(FlatFile.DET_FIELDS.INPUT_TIME.ordinal());
                if (null == jsonObject.get(JzonFile.USER_TIME) && !jzonField.equals(JzonFile.SUBMITTED) && !flatLineField.equals((FlatFile.NONE))) {
                    log.info(sectionName + FAILURE_MAPPING_FOR_SUBMITTED + jzonField + PLUS_CENTERED + flatLineField);
                } else if (null != jsonObject.get(JzonFile.USER_TIME)) {
                    jzonField = jsonObject.get(JzonFile.USER_TIME).toString();
                    log.info(sectionName + performMapCheckWithLogMessage(jzonField, flatLineField));
                } else {
                    jzonField = JzonFile.EXPECTED_NULLABLE_INPUT_TIME;
                    log.info(sectionName + CHK_MAPPING_FIELDS + jzonField + PLUS_CENTERED + flatLineField);
                }

                flatLineField = FlatParser.getField(FlatFile.DET_FIELDS.MESSAGE.ordinal());
                JSONObject jo = ((JSONObject) (jsonObject).get(JzonFile.DETAILS));
                boolean okSM = (jo).get(JzonFile.SUCCESS_MESSAGE) != null;
                boolean okFR = (jo).get(JzonFile.FAIL_REASON) != null;
                boolean okMR = (jo).get(JzonFile.MISSING_REASON) != null;
                if (okSM) {
                    jzonField = (jo).get(JzonFile.SUCCESS_MESSAGE).toString();
                } else if (okFR) {
                    jzonField = (jo).get(JzonFile.FAIL_REASON).toString();
                } else if (okMR) {
                    jzonField = ((jo).get(JzonFile.MISSING_REASON)).toString();
                }

                if (!okFR && !okMR && !okSM) {
                    log.info(sectionName + performMapCheckWithLogMessage(null, flatLineField));
                } else {
                    log.info(sectionName + performMapCheckWithLogMessage(jzonField, flatLineField));
                }

            } else if ((FlatFile.SECTION_NAME.END.name()).equals(sectionNameOfTheCurrentFlatLine)) {

                String sectionName = "[ " + FlatParser.getField(FlatFile.END_FIELDS.END.ordinal()) + " ] ";

                // EndTime – assigned after processing by the module. No mapping.
                log.info(sectionName + CHK_MAPPING_FIELDS_TARGET_ONLY + EXPECTED_EMPTY_FLAT_FIELD_RANGE_MARKER);

            } else {
                log.info("[     ] " + ERROR_UNRECOGNIZED_OUTPUT_FIELD + sectionNameOfTheCurrentFlatLine);
            }
        }

    }

    private String extractAssigneeFromJzonField(String jzonField) {
        // dummy.user_8@dsv.com -> dummy.user_8
        return jzonField.split("@")[0];
    }

    private String extractCountryCodeFromJzonField(String jzonField) {
        // DK - Denmark -> DK
        return jzonField.trim().substring(0, 2);
    }

    private static String performMapCheckWithLogMessage(String jzonField,
                                                        String flatLineField,
                                                        String... srcFieldDecorators) {

        String decoratorLeft = "";
        String decoratorRight = "";
        if (srcFieldDecorators.length == 2) {
            decoratorLeft = srcFieldDecorators[0] == null ? "" : srcFieldDecorators[0];
            decoratorRight = srcFieldDecorators[1] == null ? "" : srcFieldDecorators[1];
        }

        Pattern pattern = Pattern.compile(FlatParser.EMTY_RANGE_MARK);
        Matcher matcher = pattern.matcher(flatLineField);
        if (!matcher.matches()) {
            flatLineField = flatLineField.trim();
        } else {
            flatLineField = UNEXPECTED_EMPTY_FLAT_FIELD_RANGE_MARKER;
        }

        boolean CHK_MAPPING_FIELD = (null != jzonField) && (!"".equals(flatLineField));
        boolean ERROR_MAPPING_MISSING_SRC_VALUE = (null == jzonField) && (!"".equals(flatLineField));
        boolean ERROR_MAPPING_MISSING_TRG_VALUE = (null != jzonField) && ("".equals(flatLineField));
        boolean ERROR_MAPPING_UNDEFINED = (null == jzonField) && ("".equals(flatLineField));

        String message = "";

        if (CHK_MAPPING_FIELD)
            if (!jzonField.equals(flatLineField)) {
                message = FAILURE_MAPPING
                        + decoratorLeft + jzonField + decoratorRight
                        + PLUS_CENTERED
                        + flatLineField;
            } else {
                message = CHK_MAPPING_FIELDS
                        + decoratorLeft + jzonField + decoratorRight
                        + PLUS_CENTERED
                        + flatLineField;
            }

        if (ERROR_MAPPING_MISSING_SRC_VALUE)
            message = QualityCheckLogMessage.ERROR_MAPPING_MISSING_SRC_VALUE
                    + MISSING_FIELD
                    + PLUS_CENTERED
                    + flatLineField;

        if (ERROR_MAPPING_MISSING_TRG_VALUE)
            message = QualityCheckLogMessage.ERROR_MAPPING_MISSING_TRG_VALUE
                    + decoratorLeft + jzonField + decoratorRight
                    + PLUS_CENTERED
                    + MISSING_FIELD;

        if (ERROR_MAPPING_UNDEFINED)
            message = QualityCheckLogMessage.ERROR_MAPPING_UNDEFINED;

        return message;
    }
}
