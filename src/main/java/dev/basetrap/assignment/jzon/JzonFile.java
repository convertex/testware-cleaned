package dev.basetrap.assignment.jzon;

import dev.basetrap.assignment.util.QualityCheckLogMessage;

import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class JzonFile {

    private static Logger log = Logger.getLogger(JzonFile.class.getName());

    public static final int ARRAY_INDEX_OF_OBJECT_TO_BE_PROCESSED = 0;

    public static final String ASSIGNEE = "Assignee";

    public static final String COUNTRY = "Country";

    public static final String DATE = "Date";

    public static final String DETAILS = "Details";

    public static final String EXPECTED_NULLABLE_INPUT_TIME = "[ EXPECTED_NULLABLE_INPUT_TIME ] ";


    public static final String DIVISION = "Division";

    public static final String EORI = "EORI";

    public static final String EXPORTER = "Exporter";

    public static final String ID = "ID";

    public static final String INVOICES_COUNT = "InvoicesCount";

    public static final String PAGES_COUNT = "PagesCount";

    public static final String PROCESSING_TIME = "ProcessingTime";

    public static final String STATUS = "Status";
    public static final String SUBMITTED = "SUBMITTED";

    public static final String SUCCESS_MESSAGE = "SuccessMessage";

    public static final String MISSING_REASON = "MissingReason";

    public static final String FAIL_REASON = "FailReason";

    public static final String USER_TIME = "UserTime";

    public static boolean checkRegexpJzonFileExt(String text, String expression, boolean isSilent) {
        Pattern pattern = Pattern.compile(expression);
        Matcher matcher = pattern.matcher(text);
        if (matcher.matches()) {
            if (!isSilent) log.info(QualityCheckLogMessage.CHK_FILE_NAME_EXT + text);
        } else {
            if (!isSilent) log.info(QualityCheckLogMessage.ERROR_FILE_NAME_PATTERN_JZON + text);
        }
        return matcher.matches();
    }

    public static boolean checkRegexpInput(String text, String expression, boolean isSilent) {
        Pattern pattern = Pattern.compile(expression);
        Matcher matcher = pattern.matcher(text);
        if (matcher.matches()) {
            if (!isSilent) log.info(QualityCheckLogMessage.CHK_FILE_NAME_JZON + text);
        } else {
            log.info(QualityCheckLogMessage.ERROR_FILE_NAME_PATTERN_JZON + text);
        }
        return matcher.matches();
    }


}
