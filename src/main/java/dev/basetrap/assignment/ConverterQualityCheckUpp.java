package dev.basetrap.assignment;

import dev.basetrap.assignment.flat.FlatFinder;
import dev.basetrap.assignment.testcase.TestCase;
import org.apache.commons.cli.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;

import java.util.logging.Logger;

@Configuration
@SpringBootApplication(scanBasePackages = {
        "dev.basetrap.assignment.util",
        "dev.basetrap.assignment.flat",
        "dev.basetrap.assignment.testcase"}
)
public class ConverterQualityCheckUpp implements CommandLineRunner {

    private Logger log = Logger.getLogger(ConverterQualityCheckUpp.class.getName());

    @Autowired
    private FlatFinder flatFinder;

    @Autowired
    private TestCase testCase;

    ConverterQualityCheckUpp() {
    }

    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(ConverterQualityCheckUpp.class);
//        app.setBannerMode(Banner.Mode.OFF);
        app.run(args);
    }

    @Override
    public void run(String... args) {

        Options options = new Options();

        options.addOption("j", false, "testCorrectJzonFileName");
        options.addOption("e", false, "testJzonFileExt");
        options.addOption("f", false, "testCorrectFlatFileName");
        options.addOption("x", false, "testFlatFileExt");
        options.addOption("i", false, "testFileNameAgainstID");
        options.addOption("c", false, "testNumberOfFieldsInOutput");
        options.addOption("m", false, "testMapping");

        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("ConverterQualityCheckUpp", options);

        CommandLineParser parser = new DefaultParser();
        CommandLine cmd;
        try {
            cmd = parser.parse(options, args);
            if (cmd.hasOption("j")) {
                log.info(System.lineSeparator());
                testCase.testCorrectJzonFileName();
                log.info(System.lineSeparator());
            }

            if (cmd.hasOption("e")) {
                log.info(System.lineSeparator());
                testCase.testJzonFileExt();
                log.info(System.lineSeparator());
            }

            if (cmd.hasOption("f")) {
                log.info(System.lineSeparator());
                testCase.testCorrectFlatFileName();
                log.info(System.lineSeparator());
            }

            if (cmd.hasOption("x")) {
                log.info(System.lineSeparator());
                testCase.testFLatFileExt();
                log.info(System.lineSeparator());
            }

            if (cmd.hasOption("i")) {
                log.info(System.lineSeparator());
                testCase.testFileNameAgainstIDFlat();
                log.info(System.lineSeparator());
            }

            if (cmd.hasOption("c")) {
                log.info(System.lineSeparator());
                testCase.testNumberOfFieldsInOutput();
                log.info(System.lineSeparator());
            }

            if (cmd.hasOption("m")) {
                log.info(System.lineSeparator());
                testCase.testMapping();
                log.info(System.lineSeparator());
            }

        } catch (ParseException e) {
            log.info(e.getMessage());
        }
    }
}

