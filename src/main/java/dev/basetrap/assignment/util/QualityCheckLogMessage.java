package dev.basetrap.assignment.util;

public class QualityCheckLogMessage {

//    for list of constants type:
//    $ cat QualityCheckLogMessage.java \
//    | grep '^\s*public static final' \
//    | cut -d'=' -f1 \
//    | cut -d' ' -f9

    public static final String CHK_FILE = "[ CHK_FILE ] ";
    public static final String CHK_FILE_NAME_EXT = "[ CHK_FILE_NAME_EXT ] ";
    public static final String CHK_FILE_NAME_FLAT = "[ CHK_FILE_NAME_FLAT ] ";
    public static final String CHK_FILE_NAME_JZON = "[ CHK_FILE_NAME_JZON ] ";
    public static final String CHK_FILE_NAME_TO_ID = "[ CHK_FILE_NAME_TO_ID ] ";
    public static final String CHK_FLAT_FILE_EXT = "[ CHK_FLAT_FILE_EXT ] ";
    public static final String CHK_JZON_FILE_EXT = "[ CHK_JZON_FILE_EXT ] ";
    public static final String CHK_MAPPING_FIELDS = "[ CHK_MAPPING_FIELDS ] ";
    public static final String CHK_NUMBER_OF_FIELDS = "[ CHK_NUMBER_OF_FIELDS ] ";
    public static final String EMAIL_AT_DOMAIN = "@dsv.com";
    public static final String EMPTY_SECTION_NAME = "[     ] ";
    public static final String ERROR_EMPTY_FLAT_LINE = "[ ERROR_EMPTY_FLAT_LINE ] ";
    public static final String ERROR_FILE = "[ ERROR_FILE ] ";
    public static final String ERROR_FILE_EXT_PATTERN_FLAT = "[ ERROR_FILE_EXT_PATTERN_FLAT ] ";
    public static final String ERROR_FILE_EXT_PATTERN_JZON = "[ ERROR_FILE_EXT_PATTERN_JZON ] ";
    public static final String ERROR_FILE_NAME_PATTERN_FLAT = "[ ERROR_FILE_NAME_PATTERN_FLAT ] ";
    public static final String ERROR_FILE_NAME_PATTERN_JZON = "[ ERROR_FILE_NAME_PATTERN_JZON ] ";
    public static final String ERROR_MAPPING_MISSING_SRC_VALUE = "[ ERROR_MAPPING_MISSING_SRC_VALUE ] ";
    public static final String ERROR_MAPPING_MISSING_TRG_VALUE = "[ ERROR_MAPPING_MISSING_TRG_VALUE ] ";
    public static final String ERROR_MAPPING_UNDEFINED = "[ ERROR_MAPPING_UNDEFINED ] ";
    public static final String ERROR_MISSING_FLAT_FILE_FOR = "[ ERROR_MISSING_FLAT_FILE_FOR ] ";
    public static final String ERROR_PAIRED_WITH_IRREGULAR_FILE_NAME = "[ ERROR_PAIRED_WITH_IRREGULAR_FILE_NAME ]";
    public static final String ERROR_ROW_MISSING = "[ ERROR_ROW_MISSING ]";
    public static final String ERROR_ROW_PATTERN = "[ ERROR_ROW_PATTERN ] ";
    public static final String ERROR_UNRECOGNIZED_OUTPUT_FIELD = "[ ERROR_UNRECOGNIZED_OUTPUT_FIELD ] ";
    public static final String EXCEPTION_UNMATCHING_ID_AND_FILENAME = "[ EXCEPTION_UNMATCHING_ID_AND_FILENAME ] ";
    public static final String EXPECTED_EMPTY_FLAT_FIELD_RANGE_MARKER = "[ EXPECTED_EMPTY_FLAT_FIELD_RANGE_MARKER ] ";
    public static final String FAILURE_MAPPING = "[ FAILURE_MAPPING ] ";
    public static final String FAILURE_NUMBER_OF_FIELDS = "[ FAILURE_NUMBER_OF_FIELDS ] ";
    public static final String MISSING_FIELD = " _ ";
    public static final String UNEXPECTED_EMPTY_FLAT_FIELD_RANGE_MARKER = "[ UNEXPECTED_EMPTY_FLAT_FIELD_RANGE_MARKER ]";
    public static final String PLUS_CENTERED = " [ + ] ";
    public static final String PLUS_LEFT_CLOSED = "[ + ] ";
    public static final String CHK_MAPPING_FIELDS_TARGET_ONLY = "[ CHK_MAPPING_FIELDS_TARGET_ONLY ] " + PLUS_LEFT_CLOSED;
    public static final String CHK_MAPPING_FIELDS_USER_TIME_NULL = "[ CHK_MAPPING_FIELDS_USER_TIME_NULL ] " + PLUS_LEFT_CLOSED;
    public static final String FAILURE_MAPPING_FOR_SUBMITTED = "[ FAILURE_MAPPING_FOR_SUBMITTED ] ";
}
